import 'dotenv/config';
import { VkListener } from './listenters';
import { TelegramPoster } from './posters';
import { IPoster } from './types';
import { configService } from './services';

const TG_TOKEN = configService.getValue('TG_POSTING_BOT_TOKEN');
const TG_CHANNEL_ID = configService.getValue('TG_CHANNEL_ID');
const VK_COMMUNITY_TOKEN = configService.getValue('VK_COMMUNITY_BOT_TOKEN');

class App {
  private posters: IPoster[] = [];

  constructor() {
    /**
     * Секция для объявления экземпляров классов-постеров
     */
    if (TG_TOKEN && TG_CHANNEL_ID) {
      this.posters.push(new TelegramPoster({ token: TG_TOKEN, chatId: TG_CHANNEL_ID }));
    }

    /**
     * Секция для объявления экземпляров классов-слушателей
     */
    if (VK_COMMUNITY_TOKEN) {
      const vkListener = new VkListener({
        token: VK_COMMUNITY_TOKEN,
        onPostHandlers: this.posters.map(poster => poster.post),
      });
      vkListener.init();
    }
  }
}

new App();
