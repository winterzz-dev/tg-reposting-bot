import { PhotoAttachment } from 'vk-io';
import { selectPhotoUrl } from './select-photo-url';
import { compact } from 'lodash';

/**
 * Утилита для фильтрации вложений-изображений с каким-либо заданным размером
 * @param attachments
 */
export const filterAttachments = (attachments: PhotoAttachment[]): string[] => {
  return compact(attachments.map(attachment => selectPhotoUrl(attachment)));
};
