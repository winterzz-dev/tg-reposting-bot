import { PhotoAttachment } from 'vk-io';

const LARGE_SIZE_URL = 'large';
const MEDIUM_SIZE_URL = 'medium';
const SMALL_SIZE_URL = 'small';

const allSizesAttachment = {
  largeSizeUrl: LARGE_SIZE_URL,
  mediumSizeUrl: MEDIUM_SIZE_URL,
  smallSizeUrl: SMALL_SIZE_URL,
} as PhotoAttachment;

const largeSizeAttachment = {
  largeSizeUrl: LARGE_SIZE_URL,
} as PhotoAttachment;

const mediumSizeAttachment = {
  mediumSizeUrl: MEDIUM_SIZE_URL,
} as PhotoAttachment;

const largeSmallSizeAttachment = {
  largeSizeUrl: LARGE_SIZE_URL,
  smallSizeUrl: SMALL_SIZE_URL,
} as PhotoAttachment;

const withoutSizesAttachment = {} as PhotoAttachment;

export const photoAttachmentsMockData = {
  LARGE_SIZE_URL,
  MEDIUM_SIZE_URL,
  SMALL_SIZE_URL,
  allSizesAttachment,
  largeSizeAttachment,
  mediumSizeAttachment,
  largeSmallSizeAttachment,
  withoutSizesAttachment,
};
