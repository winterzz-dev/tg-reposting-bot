import { selectPhotoUrl } from '../select-photo-url';
import { photoAttachmentsMockData } from '../__mocks__';

describe('select-photo-url method', () => {
  it('should return large image size on attachment with all sizes', function () {
    expect(selectPhotoUrl(photoAttachmentsMockData.allSizesAttachment)).toBe(photoAttachmentsMockData.LARGE_SIZE_URL);
  });

  it('should return large image size on attachment with large size only', function () {
    expect(selectPhotoUrl(photoAttachmentsMockData.largeSizeAttachment)).toBe(photoAttachmentsMockData.LARGE_SIZE_URL);
  });

  it('should return medium image size on attachment with medium size only', function () {
    expect(selectPhotoUrl(photoAttachmentsMockData.mediumSizeAttachment)).toBe(photoAttachmentsMockData.MEDIUM_SIZE_URL);
  });

  it('should return large image size on attachment with large and small sizes only', function () {
    expect(selectPhotoUrl(photoAttachmentsMockData.largeSmallSizeAttachment)).toBe(photoAttachmentsMockData.LARGE_SIZE_URL);
  });

  it('should return undefined on attachment without any size', function () {
    expect(selectPhotoUrl(photoAttachmentsMockData.withoutSizesAttachment)).toBe(undefined);
  });
});
