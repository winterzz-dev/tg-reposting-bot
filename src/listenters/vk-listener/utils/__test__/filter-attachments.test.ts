import { photoAttachmentsMockData } from '../__mocks__';
import { filterAttachments } from '../filter-attachments';

describe('filter-attachments method', () => {
  it('should contain large image size url', function () {
    const urls = filterAttachments([photoAttachmentsMockData.largeSizeAttachment]);
    expect(urls).toEqual(expect.arrayContaining([photoAttachmentsMockData.LARGE_SIZE_URL]));
  });

  it('should return array with one children', function () {
    const urls = filterAttachments([photoAttachmentsMockData.largeSizeAttachment, photoAttachmentsMockData.withoutSizesAttachment]);
    expect(urls.length).toBe(1);
  });

  it('should return empty array for attachment without sizes', function () {
    const urls = filterAttachments([photoAttachmentsMockData.withoutSizesAttachment]);
    expect(urls.length).toBe(0);
  });
});
