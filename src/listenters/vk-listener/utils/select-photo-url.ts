import { PhotoAttachment } from 'vk-io';

/**
 * Утилита для выбора максимально доступного размера изображения
 * @param attachment - объект вложения-изображения
 */
export const selectPhotoUrl = (attachment: PhotoAttachment): string | undefined => attachment.largeSizeUrl || attachment.mediumSizeUrl || attachment.smallSizeUrl;
