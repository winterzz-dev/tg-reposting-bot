import { AttachmentType, VK } from 'vk-io';

import { filterAttachments } from './utils';
import { IListener, TListenerConstructor, TOnPostHandler } from '../../types';

interface IParams {
  token: string;
}

export class VkListener implements IListener {
  private client: VK;

  private handlers: TOnPostHandler[] = [];

  constructor({ token, onPostHandlers }: TListenerConstructor<IParams>) {
    this.client = new VK({
      token,
    });
    this.handlers = onPostHandlers;
  }

  public init() {
    this.client.updates.on('message_new', (context) => {
      const photoAttachments = context.getAllAttachments(AttachmentType.PHOTO);
      const photoLinks = filterAttachments(photoAttachments);
      const message = context.text || '';

      void this.handlers.map(handler => handler({ photoLinks, message }));
    });
    void this.client.updates.start();
  }
}
