export type TOnPostHandler = (body: IPostBody) => void;

export interface IPostBody {
  photoLinks: string[];
  message: string;
}

export interface IPoster {
  post(body: IPostBody): void;
}
