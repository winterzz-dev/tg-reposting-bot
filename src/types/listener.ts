import { TOnPostHandler } from './poster';

export type TListenerConstructor<Params> = Params & {
  onPostHandlers: TOnPostHandler[];
};

export interface IListener {
  init(): void;
}
