import { ConfigService } from './config-service';

export * from './config-service.types';
export const configService = new ConfigService();
