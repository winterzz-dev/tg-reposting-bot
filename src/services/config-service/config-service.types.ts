export interface IConfigService {
  getValue(key: string): string | undefined;
}
