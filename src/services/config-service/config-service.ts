import { IConfigService } from './config-service.types';

export class ConfigService implements IConfigService {
  private readonly config: Record<string, string | undefined>;

  constructor() {
    this.config = process.env;
  }

  public getValue(key: string): string | undefined {
    return this.config[key];
  }
}
