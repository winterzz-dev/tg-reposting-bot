import TelegramBot from 'node-telegram-bot-api';

import { IPostBody, IPoster } from '../types';
import { formatMessage } from './utils';

interface IParams {
  token: string;
  chatId: string;
}

export class TelegramPoster implements IPoster {
  private readonly bot: TelegramBot;

  private readonly chatId;

  constructor({ token, chatId }: IParams) {
    this.bot = new TelegramBot(token, { polling: true });
    this.chatId = chatId;
  }

  public post = ({ photoLinks, message }: IPostBody) => {
    void this.bot.sendMediaGroup(this.chatId, formatMessage({ photoLinks, message }));
  };
}
