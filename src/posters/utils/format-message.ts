import { isEmpty } from 'lodash';
import TelegramBot from 'node-telegram-bot-api';

import { IPostBody } from '../../types';

export const formatMessage = ({ photoLinks, message }: IPostBody) => {
  const formattedAttachments: TelegramBot.InputMedia[] = photoLinks.map((link) => {
    return {
      type: 'photo',
      media: link,
    };
  });
  if (isEmpty(formattedAttachments)) {
    return [];
  }
  formattedAttachments[0].caption = message;

  return formattedAttachments;
};
