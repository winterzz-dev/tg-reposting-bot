## build stage

FROM node:18.12.1-alpine as builder

WORKDIR /app

COPY package.json ./
COPY tsconfig.json ./
COPY src ./src

RUN npm install
RUN npm run build

## run stage

FROM node:18.12.1-alpine

WORKDIR /app

COPY package.json ./

RUN npm install --omit=dev
COPY --from=builder /app/build ./build

CMD ["npm", "start"]
